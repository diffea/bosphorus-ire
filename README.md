# Bosphorus IRE

This repo is the pre-configured project structure to be used for creating ambitious web servers using AdonisJs.

## Get Started

```bash
$ git clone git@bitbucket.org:diffea/bosphorus-ire.git
$ cd bosphorus-ire
$ npm install && npm run dev
$ mv .env.example .env
```
